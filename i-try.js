const add5 = (x) => {
  return x + 5;
};

const getFun = (fn, name) => {
  return function(arg) {
    const result = fn(arg);
    console.log('вызвана функция', name, 'с параметром', arg, 'и она вернула', result)
    return result;
  };
};
const newadd5 = getFun(add5, 'add5');

/*function digitize(n) {
  let result = '';
  for (let i = n.length - 1; i >= 0; i--) {
    result += 'n[i]';
  }
  return result.split('');
}
console.log(digitize(12345B));
*/
function newMas(H) {
  let M = [];
  for (let i = 0; i <= H; i++) {
    M.push(i);
  }
  return M;
}
//console.log(newMas(4))
const arr = N => newMas(N)

//console.log(arr(4))

([' ', 'X', ' ', ' ', 'X', ' ', 'X', ' ', ' ', 'X', ' '].map((x) => x === 'X' ? '*' : ' '));

function countDevelopers(list) {
  return list.filter(x => x.continent === 'Europe' && x.language === 'JavaScript').length;
}

function greetDevelopers(list) {
  list.map(x => x.greeting = 'Hi ' + x.firstName + ', what do you like the most about ' + x.language + '?') // не очень хорошо
  list.map(x => Object.assign({}, x, {
    //  greeting = 'Hi ' + x.firstName + ', what do you like the most about ' + x.language + '?'
  })) // правильно
  //ОБРАТИТЬ ВНИМАНИЕ НА Object.assign
  return list
}

function isRubyComing(list) {
  return list.filter(x => x.language === 'Ruby' ? true : false).length > 0

}

function getFirstPython(list) {
  let pytArr = list.filter(x => x.language === 'Python')
  return pytArr.length > 0 ? pytArr[0].firstName + ',' + pytArr[0].country : 'There will be no Python developers'
}


function countLanguages(list) {
  return list.reduce((a, b) => {
    if (a[b.language]) {
      a[b.language]++
    } else {
      a[b.language] = 1
    }
    return a;
  }, {})
}


function allContinents(list) {
  let cont = list.reduce((a, b) => {
    if (a[b.continent]) {
      a[b.continent]++
    } else {
      a[b.continent] = 1
    }
    return a;

  }, {})
  return Object.keys(cont).length === 5
}

function isAgeDiverse(list) {
  let cont = list.reduce((a, b) => {
    if (a[b.age]) {
      a[b.age]++
    } else {
      a[b.age] = 1
    }
    return a;
  }, {})

}

function addUsername(list) {
  list.map(x => x.username = x.firstName.toLowerCase() + x.lastName[0].toLowerCase() + (new Date().getFullYear() - x.age))
  return list
}


function getAverageAge(list) {
  let sum = list.map(x => x.age).reduce((a, b) => a + b);
  return Math.round(sum / list.length)
}

function findAdmin(list, lang) {
  return list.filter(x => x.language === lang).filter(x => x.githubAdmin === 'yes')
}

function isLanguageDiverse(list) {
  let lan = list.reduce((a, b) => {
    if (a[b.language]) {
      a[b.language]++
    } else {
      a[b.language] = 1
    }
    return a;
  }, {})
  let valLan = Object.keys(lan).map((k) => lan[k])
  return (Math.max(...valLan) / Math.min(...valLan)) <= 2
}

function findOddNames(list) {
  return list.filter(x => {
    const sum = x.firstName.split('').reduce((a, b) => a + b.charCodeAt(), 0) % 2 === 0
    return sum % 2 === 0
  })

}


function sortByLanguage(list) {
  return list.sort(function(a, b) {
    if (a.firstName > b.firstName) {
      return 1;
    }
    if (a.firstName < b.firstName) {
      return -1;
    }
    return 0
  }).sort(function(a, b) {
    if (a.language > b.language) {
      return 1;
    }
    if (a.language < b.language) {
      return -1;
    }
    return 0
  })
}

function sortByLanguage(list) {
  return list.sort(function(a, b) {
    if (a.language > b.language) {
      return 1;
    }
    if (a.language < b.language) {
      return -1;
    }
    if (a.firstName > b.firstName) {
      return 1;
    }
    if (a.firstName < b.firstName) {
      return -1
    }
    return 0;

  })

  function askForMissingDetails(list) {
    list.map(x => {
        function result(x) {
          let keys = Object.keys(x)
          return findKey = keys.find(key => x[key] === null)
          return
        }

      x => x.question = 'Hi, could you please provide your ' + findKey + '.'

    }
    )

  }


  console.log(askForMissingDetails([{
      firstName: null,
      lastName: 'I.',
      country: 'Argentina',
      continent: 'Americas',
      age: 35,
      language: 'Java'
    },
    {
      firstName: 'Lukas',
      lastName: 'X.',
      country: 'Croatia',
      continent: 'Europe',
      age: 35,
      language: null
    },
    {
      firstName: 'Madison',
      lastName: 'U.',
      country: 'United States',
      continent: 'Americas',
      age: 32,
      language: 'Ruby'
    }
  ]))

const range = require('../sum-range').range;
const sum = require('../sum-range').sum;

test('funсtion range make massive from  num a to num b', () => {
  expect(range(1, 10)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  expect(range(5, 12)).toEqual([5, 6, 7, 8, 9, 10, 11, 12]);
  expect(range(12, 22, 2)).toEqual([12, 14, 16, 18, 20, 22]);
});
test('make sum of massive', () => {
  expect(sum(range(1, 10))).toEqual(55);
  expect(sum(range(5, 12))).toEqual(68);
  expect(sum([0, 0, 2, 12])).toEqual(14);
  expect(sum([4, 3, 2, 1])).toEqual(10);
});

const reverseArray = require('../reverse-array').reverseArray;
const reverseArrayInPlace = require('../reverse-array').reverseArrayInPlace;


test('reverse massiv witn new array', () => {
  const array = [1, 2, 3, 4, 5];
  const newArray = reverseArray(array);
  expect(newArray).toEqual([5, 4, 3, 2, 1]);
  expect(array).toEqual([1, 2, 3, 4, 5]);
});

test('reverse massiv in the same array', () => {
  const array = [1, 2, 3, 4, 5];
  const newArray = reverseArrayInPlace(array);
  expect(array).toEqual([5, 4, 3, 2, 1]);
  expect(newArray).toEqual([5, 4, 3, 2, 1]);
});

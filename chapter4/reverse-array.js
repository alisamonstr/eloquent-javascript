function reverseArray(a) {
  const result = [];
  for (let i = a.length - 1; i >= 0; i--) {
    result.push(a[i]);
  }
  return result;
}


function reverseArrayInPlace(b) {
  for (let i = 0; i < b.length / 2; i++) {
    const pl = b[i];
    // eslint-disable-next-line no-param-reassign
    b[i] = b[b.length - i - 1];
    // eslint-disable-next-line no-param-reassign
    b[b.length - i - 1] = pl;
  }
  return b;
}

module.exports = {
  reverseArray,
  reverseArrayInPlace,
};

function range(a, b, v = 1) {
  const mas = [];
  const min = Math.min(a, b);
  const max = Math.max(a, b);
  if (v < 0) {
    for (let i = max; i >= min; i += v) {
      mas.push(i);
    }
  } else {
    for (let i = min; i <= max; i += v) {
      mas.push(i);
    }
  }
  return mas;
}

function sum(m) {
  let result = 0;
  let f = 0;
  for (let i = 0; i < m.length; i++) {
    f = m[i];
    result += f;
  }
  return result;
}

module.exports = {
  range,
  sum,
};

function arrays(arr) {
  return arr.reduce((a, b) => a.concat(b));
}

console.log(arrays([
  [1, 2, 3],
  [4, 5],
  [6],
]));

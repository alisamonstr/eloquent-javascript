function addAgeChildrens(mothers, ancestry) {
  return mothers.map((x) => {
    const childrenAge = ancestry.filter(y => x.name === y.mother).map(t => t.born - x.born);
    if (childrenAge.length > 0) {
      x.ageDifferences = childrenAge; // eslint-disable-line no-param-reassign
    }
    return x;
  });
}

function calculateAverageAgeDifference(ancestry) {
  const mothers = ancestry.filter((x) => {
    const result = ancestry.filter(y => y.mother === x.name);
    return result.length > 0;
  });

  const arrayAge = addAgeChildrens(mothers, ancestry)
  .reduce((a, b) => a.concat(b.ageDifferences), []);
  return arrayAge.reduce((a, b) => a + b) / arrayAge.length;
}

module.exports = calculateAverageAgeDifference;

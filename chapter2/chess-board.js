const strOne = (n) => {
  let st = '';
  for (let i = 0; i < n; i++) {
    if (i % 2 === 0) {
      st += '#';
    } else {
      (
            st += ' ');
    }
  }
  return `${st}\n`;
};

const strTwo = (n) => {
  let st = '';
  for (let i = 0; i < n; i++) {
    if (i % 2 === 0) {
      st += ' ';
    } else {
      st += '#';
    }
  }
  return `${st}\n`;
};


const shahmati = (n) => {
  let funk = '';
  for (let i = 0; i < n; i++) {
    if (i % 2 === 0) {
      funk += strOne(n);
    } else {
      funk += strTwo(n);
    }
  }
  return funk;
};

// console.log(shahmati(8))
module.exports = shahmati;

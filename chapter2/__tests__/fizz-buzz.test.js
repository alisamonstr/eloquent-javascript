const fizzBuzz = require('../fizz-buzz');

test('classic fizzBuzz function', () => {
  expect(fizzBuzz(3)).toEqual('Fizz');
  expect(fizzBuzz(15)).toEqual('FizzBuzz');
  expect(fizzBuzz(8)).toEqual('8');
  expect(fizzBuzz(5)).toEqual('Buzz');
});

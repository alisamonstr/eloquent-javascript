const pyramid = require('../pyramid');

test('function should returns pyramid with right size', () => {
  expect(pyramid(7)).toBe('#\n##\n###\n####\n#####\n######\n#######\n');
  expect(pyramid(5)).toBe('#\n##\n###\n####\n#####\n');
});

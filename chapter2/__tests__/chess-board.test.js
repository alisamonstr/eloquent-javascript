const chessBoard = require('../chess-board');

test('make chess board with correct size', () => {
  expect(chessBoard(8)).toBe('# # # # \n # # # #\n# # # # \n # # # #\n# # # # \n # # # #\n# # # # \n # # # #\n');
});

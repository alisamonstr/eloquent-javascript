function liniya(n) {
  let result = '';
  for (let i = 0; i <= n; i++) {
    result += '#';
  }
  return `${result}\n`;
}

function piramida(n) {
  let a = '';
  for (let i = 0; i <= n - 1; i++) {
    a += liniya(i);
  }
  return a;
}

// console.log(piramida(7));


module.exports = piramida;

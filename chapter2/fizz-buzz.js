const getValue = (i) => {
  if (i % 3 === 0 && i % 5 === 0) {
    return 'FizzBuzz';
  } else if (i % 5 === 0) {
    return 'Buzz';
  } else if (i % 3 === 0) {
    return 'Fizz';
  }
  return String(i);
};

// eslint-disable-next-line no-unused-vars
const fizzBuzz = (n) => {
  let a = '';
  for (let i = 1; i <= n; i++) {
    a += `${getValue(i)} `;
  }
  return a;
};

module.exports = getValue;

// console.log(fizzBuzz(100))

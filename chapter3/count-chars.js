function countBs(str) {
  let result = '';
  for (let i = 0; i <= str.length - 1; i++) {
    if (str.charAt(i) === 'b') {
      result += str.charAt(i);
    }
  }
  return result.length;
}

function countChar(str, char) {
  let result = '';
  for (let i = 0; i <= str.length - 1; i++) {
    if (str.charAt(i) === char) {
      result += str.charAt(i);
    }
  }
  return result.length;
}


module.exports = {
  countBs,
  countChar,
};

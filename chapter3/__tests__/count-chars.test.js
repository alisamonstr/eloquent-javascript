const countBs = require('../count-chars').countBs;
const countChar = require('../count-chars').countChar;

test('countBs should properly count \'b\' chars in string', () => {
  expect(countBs('asdasdasdbbbasdbbbbasdasb')).toBe(8);
  expect(countBs('1232r3fghfggjhgjkhgjk')).toBe(0);
});

test('countChar should properly count selected chars in string', () => {
  expect(countChar('asdasdasdbbbasdbbbbasdasb', 'b')).toBe(8);
  expect(countChar('1232r3fghfggjhgjkhgjk', '2')).toBe(2);
  expect(countChar('rrrrrrrrrrrrrrrrrr', 'a')).toBe(0);
});

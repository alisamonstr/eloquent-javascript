const isEven = require('../is-even');

test('isEven should calculate is even number or odd', () => {
  expect(isEven(50)).toBe(true);
  expect(isEven(75)).toBe(false);
  expect(isEven(0)).toBe(true);
  expect(isEven(-1)).toBe(false);
});

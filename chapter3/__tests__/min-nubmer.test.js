const min = require('../min-number');

test('min should return minimal values of numbers', () => {
  expect(min(6, 2)).toBe(2);
  expect(min(-6, 2)).toBe(-6);
  expect(min(0, 0)).toBe(0);
});


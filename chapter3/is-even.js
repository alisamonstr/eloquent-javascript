const isEven = (num) => {
  if (num < 0) {
    // eslint-disable-next-line no-param-reassign
    num = -num;
  }
  if (num === 0) {
    return true;
  } else if (num === 1) {
    return false;
  }
  return isEven(num - 2);
};

module.exports = isEven;
